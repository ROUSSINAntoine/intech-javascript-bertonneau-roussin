import startUp from "./startup.js"
let boomGrid = startUp()

window.tdclick = function tdclick(id) {

    let element = document.getElementById(`${id}`)
    console.log(element.id)
    if (element.innerText !== "F") {
        element.style.backgroundColor = "white"
        element.style.color = "black"
        element.innerText = "."
        let coord = element.id.split("/")
        if (boomGrid[coord[0]][coord[1]] === 0) {
            element.style.backgroundColor = "white"
            element.style.color = "black"
            
            if (boomGrid[Number(coord[0]) + 1] !== undefined) tdclick((Number(coord[0]) + 1) + "/" + coord[1])
            if (boomGrid[Number(coord[0]) - 1] !== undefined) tdclick((Number(coord[0]) - 1) + "/" + coord[1])
            if (boomGrid[coord[0]][Number(coord[1]) + 1] !== undefined) tdclick(coord[0] + "/" + (Number(coord[1]) + 1))
            if (boomGrid[coord[0]][Number(coord[1]) - 1] !== undefined) tdclick(coord[0] + "/" + (Number(coord[1]) - 1))

        }
        element.innerText = boomGrid[coord[0]][coord[1]]
    }
}

window.tdRightClick = function tdRightClick(event, element) {
    event.preventDefault()
    if (element.innerText !== ".") {
        if (element.innerText === "F") {
            element.innerText = ""
        } else {
            element.innerText = "F"
        }
    }
}

window.update = function update() {
    boomGrid = startUp()
}
function startUp() {
    let maxX
    let maxY
    let html = ""
    let bombeGrid = []
    let nbBombe
    const grid = document.querySelector(`#grid`)
    const diff = document.querySelector(`#difficulty`)

    console.log(diff.value)
    switch (diff.value) {
        case "easy":
            maxX = 8
            maxY = 10
            nbBombe = 10
            break;

        case "medium":
            maxX = 14
            maxY = 16
            nbBombe = 40
            break;

        case "hard":
            maxX = 25
            maxY = 27
            nbBombe = 99
            break;

        default:
            maxX = 14
            maxY = 16
            nbBombe = 40
            break;
    }

    for (let x = 0; x < maxX; x++) {
        html += `<tr id=${x}>`
        bombeGrid.push([])

        for (let y = 0; y < maxY; y++) {
            html += `<td id='${x}/${y}' onclick='tdclick(this.id);' oncontextmenu='tdRightClick(event, this)'; value='${x}/${y}'></td>`
            bombeGrid[x].push(0)
        }

        html += `</tr>`
    }

    let randGrid = []
    let boomPos = []

    for (let a = 0; a < bombeGrid.length; a++) {
        for (let b = 0; b < bombeGrid[a].length; b++) {
            randGrid.push({'x': a, 'y': b})
        }        
    }

    for (let z = 0; z < nbBombe; z++) {
        let x = getRandomInt(randGrid.length)
        boomPos.push(randGrid[x])
        bombeGrid[randGrid[x].x][randGrid[x].y] = "Booma"
        randGrid.splice(x, 1)
    }

    function getRandomInt(max) {
        return Math.floor(Math.random() * max);
    }

    for (let pos of boomPos) {
        if (bombeGrid[pos.x - 1] !== undefined && bombeGrid[pos.x - 1][pos.y - 1] !== undefined && bombeGrid[pos.x - 1][pos.y - 1] !== "Booma") bombeGrid[pos.x - 1][pos.y - 1]++
        if (bombeGrid[pos.x - 1] !== undefined && bombeGrid[pos.x - 1][pos.y] !== undefined && bombeGrid[pos.x - 1][pos.y] !== "Booma") bombeGrid[pos.x - 1][pos.y]++
        if (bombeGrid[pos.x - 1] !== undefined && bombeGrid[pos.x - 1][pos.y + 1] !== undefined && bombeGrid[pos.x - 1][pos.y + 1] !== "Booma") bombeGrid[pos.x - 1][pos.y + 1]++
        if (bombeGrid[pos.x] !== undefined && bombeGrid[pos.x][pos.y - 1] !== undefined && bombeGrid[pos.x][pos.y - 1] !== "Booma") bombeGrid[pos.x][pos.y - 1]++
        if (bombeGrid[pos.x] !== undefined && bombeGrid[pos.x][pos.y + 1] !== undefined && bombeGrid[pos.x][pos.y + 1] !== "Booma") bombeGrid[pos.x][pos.y + 1]++
        if (bombeGrid[pos.x + 1] !== undefined && bombeGrid[pos.x + 1][pos.y - 1] !== undefined && bombeGrid[pos.x + 1][pos.y - 1] !== "Booma") bombeGrid[pos.x + 1][pos.y - 1]++
        if (bombeGrid[pos.x + 1] !== undefined && bombeGrid[pos.x + 1][pos.y] !== undefined && bombeGrid[pos.x + 1][pos.y] !== "Booma") bombeGrid[pos.x + 1][pos.y]++
        if (bombeGrid[pos.x + 1] !== undefined && bombeGrid[pos.x + 1][pos.y + 1] !== undefined && bombeGrid[pos.x + 1][pos.y + 1] !== "Booma") bombeGrid[pos.x + 1][pos.y + 1]++
    }

    grid.innerHTML = html
    console.log(bombeGrid);
    return bombeGrid
};

export default startUp;
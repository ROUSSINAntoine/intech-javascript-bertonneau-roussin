let seconds = 00;
let tens = 00;
let minutes = 00;
let appendMinutes = document.querySelector("#minute")
let appendTens = document.querySelector("#miliseconds")
let appendSeconds = document.querySelector("#seconds")
let buttonStart = document.querySelector('#replay');
let buttonStop = document.querySelector('#pause');
let Interval;

window.onload = function oui() {

    clearInterval(Interval);
    Interval = setInterval(startTimer, 10);
}

buttonStart.onclick = function () {
    clearInterval(Interval);
    tens = "00";
    seconds = "00";
    minutes = "00";
    appendTens.innerHTML = tens;
    appendSeconds.innerHTML = seconds;
    appendMinutes.innerHTML = minutes;
    buttonStop.textContent = "Pause"
    clearInterval(Interval);
    Interval = setInterval(startTimer, 10);
}

buttonStop.onclick = function () {
    
    if(buttonStop.textContent === "Pause"){
        clearInterval(Interval);
        buttonStop.textContent = "Reprendre"
    }else if(buttonStop.textContent === "Reprendre"){
        buttonStop.textContent = "Pause"
        clearInterval(Interval);
        Interval = setInterval(startTimer, 10);
    }
}


function startTimer() {
    tens++;

    if (tens <= 9) {
        appendTens.innerHTML = "0" + tens;
    }

    if (tens > 9) {
        appendTens.innerHTML = tens;

    }

    if (tens > 99) {
        seconds++;
        appendSeconds.innerHTML = "0" + seconds;
        tens = 0;
        appendTens.innerHTML = "0" + 0;
    }

    if (seconds > 9) {
        appendSeconds.innerHTML = seconds;
    }

    if (seconds > 59) {
        minutes++;
        if (minutes > 9) {
            appendMinutes.innerHTML = minutes;
        } else {
            appendMinutes.innerHTML = "0" + minutes
        }

        appendSeconds.innerHTML = "0" + 0;
        seconds = 0
        appendTens.innerHTML = "0" + 0;
    }

}